from iconservice import *

TAG = 'ScoreGame'

COLOR = ['spades', 'clubs', 'hearts', 'diamonds']
CARD = {0:'C',1:'S',2:'D',3:'H'}
CONDITION = {'C':2,'S':3,'D':4,'H':5}
BET_LIMIT = 30000000000000000000

class ScoreGame(IconScoreBase):


    _USER = "user"
    _DEALER = "dealer"

    @eventlog(indexed=2)
    def BetSource(self, _from: Address, timestamp: int):
        pass

    @eventlog(indexed=2)
    def PlayerCards(self,color_user:str, card: int):
        pass


    @eventlog(indexed=2)
    def FundTransfer(self, recipient: Address, amount: int, note: str):
        pass

    @eventlog(indexed=1)
    def BetPlaced(self, amount: int):
        pass

    @eventlog(indexed=2)
    def BetResult(self,winner:str,amount:int):
        pass


    def __init__(self, db: IconScoreDatabase) -> None:
        super().__init__(db)
        self.user = ArrayDB(self._USER,db,value_type=int)
        self.dealer = ArrayDB(self._DEALER,db,value_type=int)

    def on_install(self) -> None:
        super().on_install()

    def on_update(self) -> None:
        super().on_update()

    def get_random(self, txHash: str,user_seed: str = '') -> float:
        """
       Generates a random # from tx hash, block timestamp and user provided
       seed. The block timestamp provides the source of unpredictability.

        :param txHash:
       :param user_seed: string, 'Lucky phrase' provided by user.

       :return: float,  number from [x / 100000.0 for x in range(100000)]
       """
        Logger.debug(f'Entered get_random.', TAG)
        seed = (txHash + str(self.now()) + user_seed)
        spin = (int.from_bytes(sha3_256(seed.encode()), "big") % 100000) / 100000.0
        Logger.debug(f'Result of the spin was {spin}.', TAG)
        return spin

    def call_random(self,txHash:str, user_seed: str = '') -> list:
        lis1=[]
        while 1:
            spin = (self.get_random(txHash, user_seed))
            number = int(spin * 14)
            if number != 0:
                break
        card = int(spin * 4)
        lis1.append(card)
        lis1.append(number)
        self.PlayerCards(COLOR[card],number)
        return lis1

    @external(readonly=True)
    def get_cards(self,txHash:str, user_seed: str='') -> list:
        lis1 = []
        lis = []
        length = int(len(txHash)/3)
        for x in range(1, 4):
            if x == 1:
                lis = self.call_random(txHash[:length],user_seed)
                lis1.append(lis)
            else:
                d = length * x
                a = d - length
                lis = self.call_random(txHash[a:d],user_seed)
                lis1.append(lis)
        return lis1

    @payable
    @external
    def play_game(self,user_seed:str='') -> None:
        self.BetSource(self.tx.origin, self.tx.timestamp)
        if 0 > self.msg.value > BET_LIMIT:
            revert(f'You have exceeded Bet Limit')
        amount = self.msg.value
        Logger.debug(f'Betting {amount} ', TAG)
        self.BetPlaced(amount)
        txHash = (str(bytes.hex(self.tx.hash)))
        middle_txhash = (len(txHash))/2
        middle_txhash= int(middle_txhash)
        user = self.get_cards(txHash[:middle_txhash],user_seed)
        dealer = self.get_cards(txHash[middle_txhash:],user_seed)
        value1 = 0
        for lis in user:
            color = CARD[lis[0]]
            multiplier = CONDITION[color]
            value = multiplier * lis[1]
            value1 += value
        value2 = 0
        for lis in dealer:
            color = CARD[lis[0]]
            multiplier = CONDITION[color]
            value = multiplier * lis[1]
            value2 += value

        user_point = abs(60 - value1)
        dealer_point = abs(60 - value2)
        winning_amount = amount *2
        if user_point < dealer_point:
            Logger.debug("Won", TAG)
            self.BetResult('Player',winning_amount)
            try:
                Logger.debug(f'Trying to send to ({self.tx.origin}): {winning_amount}.', TAG)
                self.icx.transfer(self.tx.origin, winning_amount)
                self.FundTransfer(self.tx.origin, winning_amount, "Player Winnings")
                Logger.debug(f'Sent winner ({self.tx.origin}) {winning_amount}.', TAG)
            except BaseException as e:
                Logger.debug(f'Send failed. Exception: {e}', TAG)
                revert('Network problem. Winnings not sent. Returning funds.')
        else:
            Logger.debug(f'Player lost. ICX retained in treasury.', TAG)
            self.BetResult('Dealer',amount)
        if user_point == dealer_point:
            Logger.debug("Draw", TAG)
            try:
                Logger.debug(f'Trying to send to ({self.tx.origin}): {amount}.', TAG)
                self.icx.transfer(self.tx.origin, amount)
                self.FundTransfer(self.tx.origin, amount, "Player Winnings")
                Logger.debug(f'Sent winner ({self.tx.origin}) {amount}.', TAG)
            except BaseException as e:
                Logger.debug(f'Send failed. Exception: {e}', TAG)
                revert('Network problem. Winnings not sent. Returning funds.')

    @payable
    def fallback(self):
        pass












    


